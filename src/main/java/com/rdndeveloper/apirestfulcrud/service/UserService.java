package com.rdndeveloper.apirestfulcrud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rdndeveloper.apirestfulcrud.entity.User;
import com.rdndeveloper.apirestfulcrud.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository repository;
	
	public List<User> getAllUsers()
	{
		return repository.findAll();
	}
	
	public User saveUser(User user)
	{
		return repository.save(user);
	}
	
	public List<User> saveUsers(List<User> users)
	{
		return repository.saveAll(users);
	}
	
	public User getUserById(int id)
	{
		return repository.findById(id).orElse(null);
	}
	
	public User getUserByName(String name)
	{
		return repository.findByName(name);
	}
	
	public User getUserByEmail(String name)
	{
		return repository.findByEmail(name);
	}
	
	public User updateUser(User user)
	{
		User check = repository.findById(user.getId()).orElse(null);
		check.setName(user.getName());
		check.setEmail(user.getEmail());
		
		return repository.save(check);		
	}
	
	public String deleteUser(int id)
	{
		repository.deleteById(id);
		return "Usuário deletado com sucesso - ID " + id;
	}
}
