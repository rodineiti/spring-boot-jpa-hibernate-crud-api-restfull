package com.rdndeveloper.apirestfulcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rdndeveloper.apirestfulcrud.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByName(String name);

	User findByEmail(String name);

}
