package com.rdndeveloper.apirestfulcrud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rdndeveloper.apirestfulcrud.entity.User;
import com.rdndeveloper.apirestfulcrud.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService service;
	
	@PostMapping("/addUser")
	public User addUser(@RequestBody User user)
	{
		return service.saveUser(user);
	}
	
	@PostMapping("/addUsers")
	public List<User> addUsers(@RequestBody List<User> users)
	{
		return service.saveUsers(users);
	}
	
	@GetMapping("/users")
	public List<User> findAllUsers()
	{
		return service.getAllUsers();
	}
	
	@GetMapping("/userById/{id}")
	public User findUserById(@PathVariable int id)
	{
		return service.getUserById(id);
	}
	
	@GetMapping("/userByName/{name}")
	public User findUserByName(@PathVariable String name)
	{
		return service.getUserByName(name);
	}
	
	@GetMapping("/userByEmail/{email}")
	public User findUserByEmail(@PathVariable String email)
	{
		return service.getUserByEmail(email);
	}
	
	@PutMapping("/updateUser")
	public User updateUser(@RequestBody User user)
	{
		return service.updateUser(user);
	}
	
	@DeleteMapping("/deleteUser/{id}")
	public String deleteUser(@PathVariable int id)
	{
		return service.deleteUser(id);
	}
}
